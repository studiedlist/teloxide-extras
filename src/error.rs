use crate::extractors::extract_chat;
use crate::ok_or_err;
use crate::ResponseType;
use futures::future::BoxFuture;
use std::sync::Arc;
use teloxide::error_handlers::ErrorHandler;
use teloxide::prelude::*;
use tokio::sync::mpsc;

#[derive(Debug)]
pub struct Error {
    pub chat: Option<ChatId>,
    pub res: Option<ResponseType>,
    pub ty: ErrorType,
}

impl std::fmt::Display for Error {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        fmt.write_fmt(format_args!("Error in chat {:?}\n{}", self.chat, self.ty))
    }
}

#[derive(Debug)]
pub enum ErrorType {
    Api(teloxide::ApiError),
    Network(teloxide::RequestError),
    Other(anyhow::Error),
}

impl std::fmt::Display for ErrorType {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::Api(err) => fmt.write_fmt(format_args!("Api error: {err}")),
            Self::Network(err) => fmt.write_fmt(format_args!("Network error: {err}")),
            Self::Other(err) => err.fmt(fmt),
        }
    }
}

impl From<anyhow::Error> for ErrorType {
    fn from(err: anyhow::Error) -> Self {
        ok_or_err(
            err.downcast::<teloxide::ApiError>()
                .map(Self::Api)
                .map_err(|err| {
                    ok_or_err(
                        err.downcast::<teloxide::RequestError>()
                            .map(|err| match err {
                                teloxide::RequestError::Api(err) => Self::Api(err),
                                err => Self::Network(err),
                            })
                            .map_err(Self::Other),
                    )
                }),
        )
    }
}

pub struct ReportingErrorHandler {
    bot: Bot,
    tx: mpsc::Sender<(Option<ChatId>, anyhow::Error)>,
    format_api_error: Option<Box<dyn Fn(teloxide::ApiError) -> String + Send + Sync>>,
    format_network_error: Option<Box<dyn Fn(teloxide::RequestError) -> String + Send + Sync>>,
    format_other_error: Option<Box<dyn Fn(anyhow::Error) -> String + Send + Sync>>,
}

impl ReportingErrorHandler {
    pub fn new(bot: Bot) -> Arc<Self> {
        let (tx, mut rx) = mpsc::channel(50);
        let s = Arc::new(Self {
            bot,
            tx,
            format_api_error: Some(Box::new(|err| format!("Api error: {err}"))),
            format_network_error: None,
            format_other_error: Some(Box::new(|err| format!("Error: {err}"))),
        });
        let se = s.clone();
        tokio::spawn(async move {
            while let Some(err) = rx.recv().await {
                se.clone().handle_error(err.into()).await;
            }
        });
        s
    }
    pub fn tx(&self) -> mpsc::Sender<(Option<ChatId>, anyhow::Error)> {
        self.tx.clone()
    }
    pub fn format_api_error(
        mut self,
        format_api_error: Option<Box<dyn Fn(teloxide::ApiError) -> String + Send + Sync>>,
    ) -> Self {
        self.format_api_error = format_api_error;
        self
    }
    pub fn format_network_error(
        mut self,
        format_network_error: Option<Box<dyn Fn(teloxide::RequestError) -> String + Send + Sync>>,
    ) -> Self {
        self.format_network_error = format_network_error;
        self
    }
    pub fn format_other_error(
        mut self,
        format_other_error: Option<Box<dyn Fn(anyhow::Error) -> String + Send + Sync>>,
    ) -> Self {
        self.format_other_error = format_other_error;
        self
    }
}

impl From<(Option<ChatId>, anyhow::Error)> for Error {
    fn from((chat, err): (Option<ChatId>, anyhow::Error)) -> Self {
        let ty = err.into();
        Self {
            chat,
            ty,
            res: None,
        }
    }
}

impl From<(Option<ChatId>, Option<ResponseType>, anyhow::Error)> for Error {
    fn from((chat, res, err): (Option<ChatId>, Option<ResponseType>, anyhow::Error)) -> Self {
        let ty = err.into();
        Self { chat, ty, res }
    }
}

impl ErrorHandler<Error> for ReportingErrorHandler {
    fn handle_error(self: Arc<Self>, err: Error) -> BoxFuture<'static, ()> {
        log::error!("{err:?}");
        Box::pin(async move {
            if let Some(chat) = err.chat {
                let msg = match err.ty {
                    ErrorType::Api(err) => self.format_api_error.as_ref().map(|f| f(err)),
                    ErrorType::Network(err) => self.format_network_error.as_ref().map(|f| f(err)),
                    ErrorType::Other(err) => self.format_other_error.as_ref().map(|f| f(err)),
                };
                match (msg, err.res) {
                    (Some(text), Some(ResponseType::Edit(msg))) => {
                        if let Err(err1) = self.bot.edit_message_text(chat, msg.id, &text).await {
                            if let Err(err2) = self.bot.edit_message_text(chat, msg.id, &text).await
                            {
                                log::error!("Unable to send error: \n- {err1:?}\n- {err2:?}");
                            }
                        }
                    }
                    (Some(text), Some(ResponseType::Send(_)) | None) => {
                        if let Err(err1) = self.bot.send_message(chat, &text).await {
                            if let Err(err2) = self.bot.send_message(chat, &text).await {
                                log::error!("Unable to send error: \n- {err1:?}\n- {err2:?}");
                            }
                        }
                    }
                    (None, _) => (),
                }
            }
        })
    }
}

use dptree::di::{DependencySupplier, Injectable};
use dptree::{from_fn_with_description, HandlerDescription};

pub fn handler<'a, F, FnArgs, Input, Descr>(f: F) -> Endpoint<'a, Input, Result<(), Error>, Descr>
where
    Input: DependencySupplier<Update> + Send + 'a,
    F: Injectable<Input, Result<(), anyhow::Error>, FnArgs> + Send + Sync + 'a,
    Descr: HandlerDescription,
{
    let f = Arc::new(f);
    from_fn_with_description(Descr::endpoint(), move |x: Input, _cont| {
        let f = Arc::clone(&f);
        let update = <Input as DependencySupplier<Update>>::get(&x);
        let res_ty = ResponseType::from(update.clone());
        let chat = extract_chat(update).map(|x| x.id);
        async move {
            let f = f.inject(&x);
            let res = f().await;
            ControlFlow::Break(res.map_err(|err| Error::from((chat, res_ty, err))))
        }
    })
}
