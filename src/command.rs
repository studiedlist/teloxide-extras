use anyhow::Context;
use derive_more::Display;
use teloxide::types::{CallbackQuery, Me, Message};
use teloxide::utils::command::ParseError;

pub trait OptionalCommand: Sized {
    fn parse(s: &str, me: &Me, msg: &Message) -> Result<Option<Self>, ParseError>;
}

pub fn matches_command<CMD: OptionalCommand>(
    msg: Option<Message>,
    callback: Option<CallbackQuery>,
    me: Me,
) -> Result<Option<CMD>, crate::error::Error> {
    let text = msg
        .as_ref()
        .map(|msg| msg.text().map(ToString::to_string))
        .or(callback.as_ref().map(|c| c.data.clone()))
        .flatten();
    let text = match text {
        Some(t) => t,
        None => return Ok(None),
    };
    let message = match msg
        .as_ref()
        .or(callback.as_ref().and_then(|c| c.message.as_ref()))
    {
        Some(m) => m,
        None => return Ok(None),
    };
    let chat_id = msg.as_ref().map(|msg| msg.chat.id).or(callback
        .as_ref()
        .and_then(|c| c.message.as_ref().map(|m| m.chat.id)));
    Ok(CMD::parse(&text, &me, message)
        .context("Unable to parse command")
        .map_err(|err| crate::error::Error::from((chat_id, err.into())))?)
}

pub mod parsing {
    use super::*;
    use frunk::hlist::{HCons, HList, HMappable};

    #[doc(hidden)]
    pub struct ParsingMapper<'a, I: Iterator<Item = &'a str>> {
        n: usize,
        split: I,
    }

    impl<'a, I: Iterator<Item = &'a str>> ParsingMapper<'a, I> {
        fn next(self) -> Self {
            Self {
                n: self.n + 1,
                split: self.split,
            }
        }
    }

    pub fn parse_command<'a, H>(s: &'a str, name: &str, list: H) -> Option<H::Output>
    where
        H: frunk::hlist::HMappable<ParsingMapper<'a, std::str::Split<'a, char>>>,
    {
        let mut split = s.split(' ');
        if split.next() == Some(name) {
            Some(list.map(ParsingMapper { n: 0, split }))
        } else {
            None
        }
    }

    #[derive(Display, Debug)]
    pub struct MatchNameError;

    impl std::error::Error for MatchNameError {}

    pub fn matches<'a>(
        name: &'a str,
    ) -> impl Fn(Option<&str>) -> Option<Result<(), ParseError>> + 'a {
        move |s| {
            s.map(|s| {
                if s == name {
                    Ok(())
                } else {
                    Err(ParseError::IncorrectFormat(Box::new(MatchNameError)))
                }
            })
        }
    }

    pub fn parse_from_str<V>(s: Option<&str>) -> Option<Result<V, ParseError>>
    where
        V: std::str::FromStr<Err: std::error::Error + Send + Sync + 'static>,
    {
        Some(
            V::from_str(s?)
                .map_err(Box::new)
                .map_err(|err| ParseError::IncorrectFormat(err))
                .map_err(Into::into),
        )
    }

    pub fn parse_optional<F, R>(
        f: F,
    ) -> impl for<'a> Fn(Option<&'a str>) -> Option<Result<Option<R>, ParseError>>
    where
        F: for<'a> Fn(Option<&'a str>) -> Option<Result<R, ParseError>>,
    {
        move |s| Some(f(s).transpose())
    }

    impl<'a, H, T, R, I> HMappable<ParsingMapper<'a, I>> for HCons<H, T>
    where
        H: Fn(Option<&str>) -> Option<Result<R, ParseError>>,
        T: frunk::hlist::HMappable<ParsingMapper<'a, I>> + HList,
        I: Iterator<Item = &'a str>,
    {
        type Output = Result<HCons<R, T::Output>, ParseError>;

        fn map(self, mut mapper: ParsingMapper<'a, I>) -> Self::Output {
            let len = self.len();
            let f = self.head;
            Ok(HCons {
                head: f(mapper.split.next()).ok_or(ParseError::TooFewArguments {
                    expected: mapper.n + len,
                    found: mapper.n,
                    message: format!("argument of type {} not found", std::any::type_name::<R>()),
                })??,
                tail: self.tail.map(mapper.next()),
            })
        }
    }
}
