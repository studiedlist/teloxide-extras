use lazy_regex::lazy_regex;

pub enum MarkdownError {
    CannotFindEnd,
    Unescaped,
}

pub struct MarkdownString(String);

impl MarkdownString {
    pub fn inner(&self) -> &str {
        &self.0
    }
    pub fn into_inner(self) -> String {
        self.0
    }
}

impl Into<String> for MarkdownString {
    fn into(self) -> String {
        self.0
    }
}

impl TryFrom<String> for MarkdownString {
    type Error = MarkdownError;

    fn try_from(s: String) -> Result<Self, Self::Error> {
        let unpaired_unescaped = lazy_regex!(r"(^|[^\\])([>|#|+|-|=])+");
        let paired_unescaped = lazy_regex!(r"(^|[^\\])([\*|_|~|`|\||{|}|\.]+)");
        let unescaped_backslash = lazy_regex!(r"^([^\\]*(\\\\)?)*$");
        let parentheses_pairs = lazy_regex!(r"^[^()]*(\([^()]*\)|[^()])*$");
        let sq_brackets_pairs = lazy_regex!(r"^[^\[\]]*(\[[^\[\]]*\]|[^\[\]])*$");

        if unpaired_unescaped.is_match(&s) {
            return Err(MarkdownError::Unescaped);
        }
        if unescaped_backslash.is_match(&s) {
            return Err(MarkdownError::Unescaped);
        }

        if !parentheses_pairs.is_match(&s.replace("\\(", "").replace("\\)", "")) {
            return Err(MarkdownError::CannotFindEnd);
        }
        if !sq_brackets_pairs.is_match(&s.replace("\\[", "").replace("\\]", "")) {
            return Err(MarkdownError::CannotFindEnd);
        }

        let paired_unescaped_count = paired_unescaped
            .captures_iter(&s)
            .filter_map(|c| c.get(2))
            .map(|m| m.as_str())
            .collect::<String>()
            .len();
        if paired_unescaped_count % 2 != 0 {
            return Err(MarkdownError::Unescaped);
        }
        Ok(Self(s))
    }
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    pub fn unescaped() {
        let valid = ".test.".to_string();
        let invalid = ".te.st.".to_string();
        matches!(MarkdownString::try_from(valid), Ok(_));
        matches!(
            MarkdownString::try_from(invalid),
            Err(MarkdownError::Unescaped)
        );
    }

    #[test]
    pub fn parentheses() {
        let valid = "()".to_string();
        let invalid = "(()".to_string();
        matches!(MarkdownString::try_from(valid), Ok(_));
        matches!(
            MarkdownString::try_from(invalid),
            Err(MarkdownError::CannotFindEnd)
        );
    }

    #[test]
    pub fn mixed() {
        let valid = "*bold \\*text*
            _italic \\*text_
            __underline__
            ~strikethrough~
            ||spoiler||
            *bold _italic bold ~italic bold strikethrough ||italic bold strikethrough spoiler||~ __underline italic bold___ bold*
            [inline URL](http://www.example.com/)
            [inline mention of a user](tg://user?id=123456789)
            ![👍](tg://emoji?id=5368324170671202286)
            `inline fixed-width code`
            ```
            pre-formatted fixed-width code block
            ```
            ```python
            pre-formatted fixed-width code block written in the Python programming language
        ```".to_string();
        matches!(MarkdownString::try_from(valid), Ok(_));
        let invalid = "Hello {}. Please confirm that you are human".to_string();
        matches!(
            MarkdownString::try_from(invalid),
            Err(MarkdownError::Unescaped)
        );
    }
}
