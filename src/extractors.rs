use chrono::{DateTime, Utc};
use std::borrow::Borrow;
use teloxide::types::{Chat, Update, UpdateKind, User};

pub fn extract_chat<U: Borrow<Update>>(upd: U) -> Option<Chat> {
    match &upd.borrow().kind {
        UpdateKind::Message(msg)
        | UpdateKind::EditedMessage(msg)
        | UpdateKind::ChannelPost(msg)
        | UpdateKind::EditedChannelPost(msg) => Some(msg.chat.clone()),
        UpdateKind::ChatMember(upd) | UpdateKind::MyChatMember(upd) => Some(upd.chat.clone()),
        UpdateKind::ChatJoinRequest(req) => Some(req.chat.clone()),
        _ => None,
    }
}

pub fn extract_date<U: Borrow<Update>>(upd: U) -> Option<DateTime<Utc>> {
    match &upd.borrow().kind {
        UpdateKind::Message(msg)
        | UpdateKind::EditedMessage(msg)
        | UpdateKind::ChannelPost(msg)
        | UpdateKind::EditedChannelPost(msg) => Some(msg.date),
        UpdateKind::ChatMember(upd) | UpdateKind::MyChatMember(upd) => Some(upd.date),
        UpdateKind::ChatJoinRequest(req) => Some(req.date),
        _ => None,
    }
}

pub fn extract_sender<U: Borrow<Update>>(upd: U) -> Option<User> {
    match &upd.borrow().kind {
        UpdateKind::Message(msg)
        | UpdateKind::EditedMessage(msg)
        | UpdateKind::ChannelPost(msg)
        | UpdateKind::EditedChannelPost(msg) => msg.from().cloned(),
        UpdateKind::ChatMember(upd) | UpdateKind::MyChatMember(upd) => Some(upd.from.clone()),
        UpdateKind::ChatJoinRequest(req) => Some(req.from.clone()),
        UpdateKind::InlineQuery(q) => Some(q.from.clone()),
        UpdateKind::ChosenInlineResult(r) => Some(r.from.clone()),
        UpdateKind::CallbackQuery(c) => Some(c.from.clone()),
        UpdateKind::ShippingQuery(q) => Some(q.from.clone()),
        UpdateKind::PreCheckoutQuery(q) => Some(q.from.clone()),
        UpdateKind::PollAnswer(a) => Some(a.user.clone()),
        _ => None,
    }
}
