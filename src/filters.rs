use cached::proc_macro::cached;
use cached::TimedSizedCache;
use teloxide::prelude::*;

pub mod dptree {

    use teloxide::prelude::*;

    pub async fn is_admin(msg: Message, bot: Bot) -> Result<bool, anyhow::Error> {
        match msg.from() {
            Some(u) => super::is_admin(msg.chat.id, u.id, &bot).await,
            None => Ok(false),
        }
    }
}

#[cached(
    type = "TimedSizedCache<(ChatId, UserId), bool>",
    create = "{ TimedSizedCache::with_size_and_lifespan(100, 15) }",
    convert = r#"{ (chat_id, user_id) }"#,
    result = true
)]
pub async fn is_admin(chat_id: ChatId, user_id: UserId, bot: &Bot) -> Result<bool, anyhow::Error> {
    let admins = bot.get_chat_administrators(chat_id).await?;
    Ok(admins.iter().any(|m| m.user.id == user_id))
}
