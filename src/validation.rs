pub mod markdown;

use markdown::MarkdownString;
use teloxide::payloads::SendMessageSetters;
use teloxide::requests::Requester;
use teloxide::types::MessageEntity;
use teloxide::types::MessageId;
use teloxide::types::ParseMode;
use teloxide::types::Recipient;
use teloxide::types::ReplyMarkup;
use teloxide::Bot;

pub enum ParseModeExt {
    Plain(String),
    MarkdownV2(MarkdownString),
}

impl ParseModeExt {
    pub fn parse_mode(&self) -> Option<ParseMode> {
        match self {
            Self::Plain(_) => None,
            Self::MarkdownV2(_) => Some(ParseMode::MarkdownV2),
        }
    }
}

impl From<String> for ParseModeExt {
    fn from(s: String) -> Self {
        Self::Plain(s)
    }
}

impl From<&str> for ParseModeExt {
    fn from(s: &str) -> Self {
        Self::Plain(s.to_string())
    }
}

impl From<MarkdownString> for ParseModeExt {
    fn from(s: MarkdownString) -> Self {
        Self::MarkdownV2(s)
    }
}

impl Into<String> for ParseModeExt {
    fn into(self) -> String {
        match self {
            Self::Plain(s) => s,
            Self::MarkdownV2(s) => s.into_inner(),
        }
    }
}

pub trait RequesterExt: Requester {
    fn send_message_ext<C, P: Into<ParseModeExt>>(&self, chat_id: C, text: P) -> Self::SendMessage
    where
        C: Into<Recipient>;
}
impl RequesterExt for Bot {
    fn send_message_ext<C, P: Into<ParseModeExt>>(&self, chat_id: C, text: P) -> Self::SendMessage
    where
        C: Into<Recipient>,
    {
        let text = text.into();
        let parse_mode = text.parse_mode();
        let text: String = text.into();
        let mut s = Requester::send_message(self, chat_id, text);
        if let Some(m) = parse_mode {
            s = s.parse_mode(m)
        }
        s
    }
}

pub struct SendMessageExt {
    pub chat_id: Recipient,
    pub message_thread_id: Option<i32>,
    pub parse_mode: ParseModeExt,
    pub entities: Option<Vec<MessageEntity>>,
    pub disable_web_page_preview: Option<bool>,
    pub disable_notification: Option<bool>,
    pub protect_content: Option<bool>,
    pub reply_to_message_id: Option<MessageId>,
    pub allow_sending_without_reply: Option<bool>,
    pub reply_markup: Option<ReplyMarkup>,
}

impl SendMessageExt {
    pub fn new(chat_id: impl Into<Recipient>, text: impl Into<ParseModeExt>) -> Self {
        Self {
            chat_id: chat_id.into(),
            parse_mode: text.into(),
            message_thread_id: None,
            entities: None,
            disable_web_page_preview: None,
            disable_notification: None,
            protect_content: None,
            reply_to_message_id: None,
            allow_sending_without_reply: None,
            reply_markup: None,
        }
    }
}
