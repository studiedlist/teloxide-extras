use crate::extractors::extract_chat;
use dptree::{
    di::{Asyncify, DependencySupplier, Injectable, Insert},
    from_fn_with_description, Endpoint, Handler, HandlerDescription,
};
use std::ops::ControlFlow;
use std::sync::Arc;
use teloxide::types::Update;

pub mod fallible {
    use super::*;

    #[must_use]
    pub fn filter_async_with_description<'a, Pred, Input, Error, Output, FnArgs, Descr>(
        description: Descr,
        pred: Pred,
    ) -> Handler<'a, Input, Result<Output, Error>, Descr>
    where
        Pred: Injectable<Input, Result<bool, Error>, FnArgs> + Send + Sync + 'a,
        Input: Send + 'a,
        Error: Send + 'a,
        Output: Send + 'a,
    {
        let pred = Arc::new(pred);

        from_fn_with_description(description, move |event, cont| {
            let pred = Arc::clone(&pred);

            async move {
                let pred = pred.inject(&event);
                let res = pred().await;
                drop(pred);

                match res {
                    Ok(true) => cont(event).await,
                    Ok(false) => ControlFlow::Continue(event),
                    Err(err) => ControlFlow::Break(Err(err)),
                }
            }
        })
    }

    #[must_use]
    #[track_caller]
    pub fn filter_async<'a, Pred, Input, Error, Output, FnArgs, Descr>(
        pred: Pred,
    ) -> Handler<'a, Input, Result<Output, Error>, Descr>
    where
        Pred: Injectable<Input, Result<bool, Error>, FnArgs> + Send + Sync + 'a,
        Input: Send + 'a,
        Output: Send + 'a,
        Error: Send + 'a,
        Descr: HandlerDescription,
    {
        filter_async_with_description(Descr::filter_async(), pred)
    }

    #[must_use]
    pub fn filter_with_description<'a, Pred, Input, Error, Output, FnArgs, Descr>(
        description: Descr,
        pred: Pred,
    ) -> Handler<'a, Input, Result<Output, Error>, Descr>
    where
        Asyncify<Pred>: Injectable<Input, Result<bool, Error>, FnArgs> + Send + Sync + 'a,
        Input: Send + 'a,
        Error: Send + 'a,
        Output: Send + 'a,
    {
        filter_async_with_description(description, Asyncify(pred))
    }

    #[must_use]
    #[track_caller]
    pub fn filter<'a, Pred, Input, Error, Output, FnArgs, Descr>(
        pred: Pred,
    ) -> Handler<'a, Input, Result<Output, Error>, Descr>
    where
        Asyncify<Pred>: Injectable<Input, Result<bool, Error>, FnArgs> + Send + Sync + 'a,
        Input: Send + 'a,
        Error: Send + 'a,
        Output: Send + 'a,
        Descr: HandlerDescription,
    {
        filter_with_description(Descr::filter(), pred)
    }

    #[must_use]
    pub fn filter_map_async_with_description<
        'a,
        Projection,
        Input,
        NewType,
        Error,
        Output,
        Args,
        Descr,
    >(
        description: Descr,
        proj: Projection,
    ) -> Handler<'a, Input, Result<Output, Error>, Descr>
    where
        Input: Clone,
        Projection: Injectable<Input, Result<Option<NewType>, Error>, Args> + Send + Sync + 'a,
        Input: Insert<NewType> + Send + 'a,
        Output: Send + 'a,
        Error: Send + 'a,
        NewType: Send,
    {
        let proj = Arc::new(proj);

        from_fn_with_description(description, move |container: Input, cont| {
            let proj = Arc::clone(&proj);

            async move {
                let proj = proj.inject(&container);
                let res = proj().await;
                std::mem::drop(proj);

                match res {
                    Ok(Some(new_type)) => {
                        let mut intermediate = container.clone();
                        intermediate.insert(new_type);
                        match cont(intermediate).await {
                            ControlFlow::Continue(_) => ControlFlow::Continue(container),
                            ControlFlow::Break(result) => ControlFlow::Break(result),
                        }
                    }
                    Ok(None) => ControlFlow::Continue(container),
                    Err(err) => ControlFlow::Break(Err(err)),
                }
            }
        })
    }

    #[must_use]
    pub fn filter_map_with_description<'a, Projection, Input, Error, Output, NewType, Args, Descr>(
        description: Descr,
        proj: Projection,
    ) -> Handler<'a, Input, Result<Output, Error>, Descr>
    where
        Input: Clone,
        Asyncify<Projection>:
            Injectable<Input, Result<Option<NewType>, Error>, Args> + Send + Sync + 'a,
        Input: Insert<NewType> + Send + 'a,
        Output: Send + 'a,
        Error: Send + 'a,
        NewType: Send,
    {
        filter_map_async_with_description(description, Asyncify(proj))
    }

    #[must_use]
    #[track_caller]
    pub fn filter_map_async<'a, Projection, Input, Error, Output, NewType, Args, Descr>(
        proj: Projection,
    ) -> Handler<'a, Input, Result<(), Error>, Descr>
    where
        Input: Clone,
        Projection: Injectable<Input, Result<Option<NewType>, Error>, Args> + Send + Sync + 'a,
        Input: Insert<NewType> + Send + 'a,
        Output: Send + 'a,
        Error: Send + 'a,
        Descr: HandlerDescription,
        NewType: Send,
    {
        filter_map_async_with_description(Descr::filter_map_async(), proj)
    }

    #[must_use]
    #[track_caller]
    pub fn filter_map<'a, Projection, Input, Error, Output, NewType, Args, Descr>(
        proj: Projection,
    ) -> Handler<'a, Input, Result<Output, Error>, Descr>
    where
        Input: Clone,
        Asyncify<Projection>:
            Injectable<Input, Result<Option<NewType>, Error>, Args> + Send + Sync + 'a,
        Input: Insert<NewType> + Send + 'a,
        Output: Send + 'a,
        Descr: HandlerDescription,
        NewType: Send,
        Error: Send + 'a,
    {
        filter_map_with_description(Descr::filter_map(), proj)
    }

    #[must_use]
    pub fn map_async_with_description<'a, Projection, Input, Output, Error, NewType, Args, Descr>(
        description: Descr,
        proj: Projection,
    ) -> Handler<'a, Input, Result<Output, Error>, Descr>
    where
        Input: Clone,
        Projection: Injectable<Input, Result<NewType, Error>, Args> + Send + Sync + 'a,
        Input: Insert<NewType> + Send + 'a,
        Output: 'a,
        Error: Send + 'a,
        Descr: HandlerDescription,
        NewType: Send,
    {
        let proj = Arc::new(proj);

        from_fn_with_description(description, move |container: Input, cont| {
            let proj = Arc::clone(&proj);

            async move {
                let proj = proj.inject(&container);
                let res = proj().await;
                std::mem::drop(proj);

                let mut intermediate = container.clone();
                match res {
                    Ok(val) => {
                        intermediate.insert(val);
                        match cont(intermediate).await {
                            ControlFlow::Continue(_) => ControlFlow::Continue(container),
                            ControlFlow::Break(result) => ControlFlow::Break(result),
                        }
                    }
                    Err(err) => ControlFlow::Break(Err(err)),
                }
            }
        })
    }

    #[must_use]
    pub fn map_with_description<'a, Projection, Input, Output, Error, NewType, Args, Descr>(
        description: Descr,
        proj: Projection,
    ) -> Handler<'a, Input, Result<Output, Error>, Descr>
    where
        Input: Clone,
        Asyncify<Projection>: Injectable<Input, Result<NewType, Error>, Args> + Send + Sync + 'a,
        Input: Insert<NewType> + Send + 'a,
        Output: 'a,
        Error: Send + 'a,
        Descr: HandlerDescription,
        NewType: Send,
    {
        map_async_with_description(description, Asyncify(proj))
    }

    #[must_use]
    #[track_caller]
    pub fn map_async<'a, Projection, Input, Output, Error, NewType, Args, Descr>(
        proj: Projection,
    ) -> Handler<'a, Input, Result<Output, Error>, Descr>
    where
        Input: Clone,
        Projection: Injectable<Input, Result<NewType, Error>, Args> + Send + Sync + 'a,
        Input: Insert<NewType> + Send + 'a,
        Output: 'a,
        Error: Send + 'a,
        Descr: HandlerDescription,
        NewType: Send,
    {
        map_async_with_description(Descr::map_async(), proj)
    }

    #[must_use]
    #[track_caller]
    pub fn map<'a, Projection, Input, Output, Error, NewType, Args, Descr>(
        proj: Projection,
    ) -> Handler<'a, Input, Result<Output, Error>, Descr>
    where
        Input: Clone,
        Asyncify<Projection>: Injectable<Input, Result<NewType, Error>, Args> + Send + Sync + 'a,
        Input: Insert<NewType> + Send + 'a,
        Output: 'a,
        Error: Send + 'a,
        Descr: HandlerDescription,
        NewType: Send,
    {
        map_with_description(Descr::map(), proj)
    }
}

pub fn handler<'a, F, FnArgs, Input, Descr>(
    f: F,
) -> Endpoint<'a, Input, Result<(), crate::error::Error>, Descr>
where
    Input: DependencySupplier<Update> + Send + 'a,
    F: Injectable<Input, Result<(), anyhow::Error>, FnArgs> + Send + Sync + 'a,
    Descr: HandlerDescription,
{
    let f = Arc::new(f);
    from_fn_with_description(Descr::endpoint(), move |x: Input, _cont| {
        let f = Arc::clone(&f);
        let chat = extract_chat(<Input as DependencySupplier<Update>>::get(&x)).map(|x| x.id);
        async move {
            let f = f.inject(&x);
            let res = f().await;
            ControlFlow::Break(res.map_err(|err| crate::error::Error::from((chat, err))))
        }
    })
}
