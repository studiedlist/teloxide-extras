pub mod command;
pub mod dptree;
pub mod error;
pub mod extractors;
pub mod filters;
pub mod validation;

pub(crate) fn ok_or_err<T>(res: Result<T, T>) -> T {
    match res {
        Ok(t) => t,
        Err(t) => t,
    }
}

use std::borrow::Borrow;
use teloxide::types::ChatId;
use teloxide::types::Message;
use teloxide::types::Update;
use teloxide::types::UpdateKind;

#[derive(Clone, Debug, PartialEq)]
pub enum ResponseType {
    Edit(Message),
    Send(ChatId),
}

impl ResponseType {
    pub fn chat(&self) -> ChatId {
        match self {
            Self::Edit(msg) => msg.chat.id,
            Self::Send(id) => *id,
        }
    }
    pub fn from<U: Borrow<Update>>(upd: U) -> Option<Self> {
        let upd = upd.borrow();
        match &upd.kind {
            UpdateKind::Message(msg)
            | UpdateKind::EditedMessage(msg)
            | UpdateKind::ChannelPost(msg)
            | UpdateKind::EditedChannelPost(msg) => Some(Self::Send(msg.chat.id)),
            UpdateKind::InlineQuery(_)
            | UpdateKind::ChosenInlineResult(_)
            | UpdateKind::ShippingQuery(_)
            | UpdateKind::PreCheckoutQuery(_)
            | UpdateKind::Poll(_)
            | UpdateKind::PollAnswer(_)
            | UpdateKind::Error(_) => None,
            UpdateKind::MyChatMember(m) | UpdateKind::ChatMember(m) => Some(Self::Send(m.chat.id)),
            UpdateKind::ChatJoinRequest(r) => Some(Self::Send(r.chat.id)),
            UpdateKind::CallbackQuery(q) => q.message.clone().map(Self::Edit),
        }
    }
}
